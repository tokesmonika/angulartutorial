var app = angular.module('myApp');
app.controller('myCtrl', function($scope, $http, $window, CarService) {
    var CAR_LOCAL_STORAGE_KEY = "carRecords";

    $scope.car = {};

    $scope.addNewRecord = function(car, form) {
        if (form.$valid) {
            var lastNumber = CarService.getLastSavedCarNumber();
            $scope.car.carNumber = lastNumber;
            CarService.addNew($scope.car);
            $window.location.href = 'manage.html';
        }
    }
    $scope.returnToManage = function() {
        $window.location.href = 'manage.html';
    }
});
