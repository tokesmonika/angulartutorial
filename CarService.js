var app = angular.module('myApp');
app.factory('CarService', function(localStorageService) {
    var myFunctions;
    var LS_KEY = "carRecords";
    var lastCarSavedNumber = "lastCarSaved";

    _getDefaultCars = function() {
        return [{
            carNumber: "1",
            model: "1310",
            brand: "Dacia",
            year: "1993",
            kilometers: "1.230.000",
            color: "black",
            price: "500$",
            specifications: "Works fine"
        }, {
            carNumber: "2",
            model: "Golf 6",
            brand: "Volskwagen",
            year: "2012",
            kilometers: "23.000",
            color: "pink",
            price: "4500$",
            specifications: "Perfect in any situation"
        }, {
            carNumber: "3",
            model: "A6",
            brand: "Audi",
            year: "2004",
            kilometers: "100.000",
            color: "green",
            price: "5000$",
            specifications: "For gentlemens"
        }, {
            carNumber: "4",
            model: "octavia",
            brand: "Skoda",
            year: "2006",
            kilometers: "4.325.437",
            color: "white",
            price: "1500$",
            specifications: "No destination is unreachable with it"
        }];
    };

    _getAll = function() {
        var carsArray = localStorageService.get(LS_KEY);
        if (carsArray == null) {
            return _getDefaultCars();
        }

        return carsArray;
    };

    _setLast = function(carNumber){
        localStorageService.set(lastCarSavedNumber, carNumber);
    }

    _getLast = function(){
        var lastNumber = localStorageService.get(lastCarSavedNumber);  
        return ++lastNumber;
    }
    _getById = function(carId) {
        var carArr = _getAll();
        var selCar = {};

        for (var i = carArr.length - 1; i >= 0; i--) {
            if (carArr[i].carNumber == carId)
                selCar = carArr[i];
        }
        return selCar;
    }
    _deleteOne = function(carId) {
        var oldArr = _getAll();
        var newArr = [];

        for (var  i = 0; i < oldArr.length; i++) {
            if (oldArr[i].carNumber != carId && oldArr[i].carNumber != null)
                newArr.push(oldArr[i]);
        }
        localStorageService.set(LS_KEY, newArr);
    };
    _addNew = function(newCar) {
        var carsArray = localStorageService.get(LS_KEY);
        if (carsArray == null) {
            carsArray = _getDefaultCars();
        }
        carsArray.push(newCar);
        localStorageService.set(LS_KEY, carsArray);
        _setLast(newCar.carNumber);
    };

    _updateCar = function(car){
        var carsArray = localStorageService.get(LS_KEY);
        if (carsArray == null) {
            carsArray = _getDefaultCars();
        }
        for (var i = carsArray.length - 1; i >= 0; i--) {
            if (carsArray[i].carNumber == car.carNumber)
                carsArray[i] = car;
        }
        localStorageService.set(LS_KEY, carsArray);
    }


    return myFunctions = {
        getAll: _getAll,
        addNew: _addNew,
        getById: _getById,
        deleteOne: _deleteOne,
        updateCar: _updateCar,
        getLastSavedCarNumber: _getLast
    };
});