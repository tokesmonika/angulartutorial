'use strict';
var myApp = angular.module('myApp');

myApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
        
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

myApp.controller('carsCtrl', function($scope, $window,$compile, $timeout, CarService) {
    var CAR_LOCAL_STORAGE_KEY = "carRecords";
   
    $scope.allCars = CarService.getAll();

    $scope.deleteCar = function(car) {
        CarService.deleteOne(car.carNumber);
        $scope.allCars = CarService.getAll();
    }

    $scope.editCar = function() {
        $window.location.href = 'update.html?carNumber=' + $scope.selected_car.carNumber;
    }

    $scope.updateCar = function(car) {
        $window.location.href = 'update.html?carNumber=' + car.carNumber;
    }

    $scope.openAddPage = function() {
        $window.location.href = 'add.html';
    }




    var vm = this;

  vm.disabled = undefined;
  vm.searchEnabled = undefined;

  vm.setInputFocus = function (){
    $scope.$broadcast('UiSelectDemo1');
  };

  vm.enable = function() {
    vm.disabled = false;
  };

  vm.disable = function() {
    vm.disabled = true;
  };

  vm.enableSearch = function() {
    vm.searchEnabled = true;
  };

  vm.disableSearch = function() {
    vm.searchEnabled = false;
  };

  vm.clear = function() {
    vm.person.selected = undefined;
    vm.address.selected = undefined;
    vm.country.selected = undefined;
  };

  vm.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  vm.firstLetterGroupFn = function (item){
      return item.name[0];
  };

  vm.reverseOrderFilterFn = function(groups) {
    return groups.reverse();
  };






});