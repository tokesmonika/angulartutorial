'use strict';
var app = angular.module('myApp');
app.controller('myCtrl', function($scope, $http, $window, CarService, DriverService, PathService) {
    var CAR_LOCAL_STORAGE_KEY = "carRecords";

    var paramArray = PathService.searchParams();
    if (paramArray[0] == "carNumber")
        var carNumber = paramArray[1];

    $scope.car = CarService.getById(carNumber);
    $scope.allDrivers = DriverService.getAllForCar(carNumber);

    $scope.updateCarRecord = function(form) {
        if (form.$valid) {
            CarService.updateCar($scope.car);
            $window.location.href = 'manage.html';
        }
    }
    $scope.returnToManage = function() {
        $window.location.href = 'manage.html';
    }
    $scope.openAddDriverPage = function() {
        $window.location.href = 'addDriver.html?carNumber=' + $scope.car.carNumber;
    }
    $scope.deleteDriver = function(driver) {
        DriverService.deleteOne(driver.carNumber, driver.driverNumber);
        $scope.allDrivers = DriverService.getAllForCar(driver.carNumber);
    }
    $scope.updateDriver = function(driver) {
        $window.location.href = 'updateDriver.html?carNumber=' + driver.carNumber + '&driverNumber=' + driver.driverNumber;
    }
});