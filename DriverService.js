var app = angular.module('myApp');
app.factory('DriverService', function(localStorageService) {
    var myFunctions;
    var LS_KEY = "driverRecords";
    var lastDriverNumberSaved = "lastDriverSaved";


   _getDefaultDriver = function() {
        return [{
            carNumber: "2",
            driverNumber:"1",
            driverSurname:"Popescu",
            driverForename:"Marinel",
            birthDate:"03/05/1980"
        }];
    };

    _getAllDrivers = function() {
        var driverArray = localStorageService.get(LS_KEY);
        if (driverArray == null) {
            return _getDefaultDriver();
        }
        return driverArray;
    };

    _getAllForCar = function(carId) {
        var driverArr = _getAllDrivers();
        var goodDrivers = [];

        for (var i = driverArr.length - 1; i >= 0; i--) {
            if (driverArr[i].carNumber == carId)
                goodDrivers.push(driverArr[i]);
        }
        return goodDrivers;
    };

    _deleteOne = function(carNumber,driverId) {
        var oldArr = _getAllDrivers();
        var newArr = [];

        for (var i = oldArr.length - 1; i >= 0; i--) {
            if (!(oldArr[i].carNumber == carNumber && oldArr[i].driverNumber == driverId))
                newArr.push(oldArr[i]);
        }
        localStorageService.set(LS_KEY, newArr);
    };

    _addNew = function(newDriver) {
        var driversArray = localStorageService.get(LS_KEY);
        if (driversArray == null) {
            driversArray = _getAllForCar(newDriver.carNumber);
        }
        driversArray.push(newDriver);
        localStorageService.set(LS_KEY, driversArray);
        _setLast(newDriver.driverNumber);
    };

    _updateDriver = function(driver){
        var driverArray = localStorageService.get(LS_KEY);
        if (driverArray == null) {
            driverArray = _getAllDrivers();
        }
        for (var i = driverArray.length - 1; i >= 0; i--) {
            if (driverArray[i].carNumber == driver.carNumber && driverArray[i].driverNumber == driver.driverNumber)
                driverArray[i] = driver;
        }
        localStorageService.set(LS_KEY, driverArray);
    }
    _setLast = function(carNumber){
        localStorageService.set(lastDriverNumberSaved, carNumber);
    }


    _getLast = function(){
        var lastNumber = localStorageService.get(lastDriverNumberSaved);  
        return ++lastNumber;
    }
    _getDriverById = function(driverId) {
        var driverArr = _getAllDrivers();
        var selDriver = {};

        for (var i = driverArr.length - 1; i >= 0; i--) {
            if (driverArr[i].driverNumber == driverId)
                selDriver = driverArr[i];
        }
        return selDriver;
    }

    return myFunctions = {
        getAllDrivers: _getAllDrivers,
        getAllForCar: _getAllForCar,
        addNew: _addNew,
        deleteOne: _deleteOne,
        updateDriver: _updateDriver,
        getLastSavedDriver : _getLast,
        getById: _getDriverById
    };
});


