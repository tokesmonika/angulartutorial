[{
    "CarNumber": "1",
    "Model": "1310",
    "Brand": "Dacia",
    "Year": "1993",
    "Kilometers": "1.230.000",
    "Color": "black",
    "Price": "500$",
    "Specifications": "Works fine"
}, {
    "CarNumber": "2",
    "Model": "Golf 6",
    "Brand": "Volskwagen",
    "Year": "2012",
    "Kilometers": "23.000",
    "Color": "pink",
    "Price": "4500$",
    "Specifications": "Perfect in any situation"
}, {
    "CarNumber": "3",
    "Model": "A6",
    "Brand": "Audi",
    "Year": "2004",
    "Kilometers": "100.000",
    "Color": "green",
    "Price": "5000$",
    "Specifications": "For gentlemens"
}, {
    "CarNumber": "4",
    "Model": "octavia",
    "Brand": "Skoda",
    "Year": "2006",
    "Kilometers": "4.325.437",
    "Color": "white",
    "Price": "1500$",
    "Specifications": "No destination is unreachable with it"
}]