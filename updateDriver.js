var app = angular.module('myApp');
app.controller('UpdateDriverCtrl', function($scope, $http, $window, DriverService, PathService) {
    var CAR_LOCAL_STORAGE_KEY = "driverRecords";

    $scope.driver = {};
    var paramArray = PathService.searchParams();
    if (paramArray[0] == "carNumber")
        $scope.driver.carNumber = paramArray[1];
    if (paramArray[2] == "driverNumber")
        $scope.driver = DriverService.getById(paramArray[3]);
    else $window.location.href = 'update.html?carNumber=' + $scope.driver.carNumber;

    $scope.dateOptions = {
        dateFormat: "dd-M-yy"
    };
    $scope.updateDriver = function(form) {
        if (form.$valid) {
            DriverService.updateDriver($scope.driver);
            $window.location.href = 'update.html?carNumber=' + $scope.driver.carNumber;
        }
    }
    $scope.returnToUpdateCar = function() {
        $window.location.href = 'update.html?carNumber=' + $scope.driver.carNumber;
    }
});