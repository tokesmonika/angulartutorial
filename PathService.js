var app = angular.module('myApp');
app.factory('PathService', function($window) {
    var myFunctions;

    _mySearch = function() {
        var param = {};
        var i = 0, j = 0;
        var param;
        var list =[];
        var url = $window.location.search.substring(1);
        if (url.indexOf("&") > -1) {
            var paramArray = url.split("&");
            for (i = 0; i < paramArray.length; i++) {
                param = paramArray[i].split("=");
                list[j] = param[0];
                list[j + 1] = param[1];
                j += 2;
            }
        } else {
            list = url.split("=");
        }
        return list;
    };

    return myFunctions = {
        searchParams: _mySearch
    };
});