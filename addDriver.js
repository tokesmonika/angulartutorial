var app = angular.module('myApp');
app.controller('drCtrl', function($scope, $http, $window, DriverService, PathService) {
    var CAR_LOCAL_STORAGE_KEY = "driverRecords";

    $scope.driver = {};
    var paramArray = PathService.searchParams();
    if (paramArray[0] == "carNumber")
        $scope.driver.carNumber = paramArray[1];

    $scope.addNewRecord = function( driver, form) {
        if (form.$valid) {
            var lastNumber = DriverService.getLastSavedDriver();
            $scope.driver.driverNumber = lastNumber;
            DriverService.addNew($scope.driver);
            $window.location.href = 'update.html?carNumber=' + $scope.driver.carNumber;
        }
    }
    $scope.returnToUpdateCar = function() {
        $window.location.href = 'update.html?carNumber=' + $scope.driver.carNumber;
    }
});